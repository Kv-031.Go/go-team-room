// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package server

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/websocket"
	"go-team-room/controllers/messages"
	"io/ioutil"
	"net/http"
	"time"
)

var ChatRoomName = "hum_chat_room"

const (
	// Time allowed to write a message to the peer.
	writeWait = 86400 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 15 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size, here 1 kB
	maxMessageSize = 1024 * 1024
)

func init() {

	if _, err := SetNewRoom(ChatRoomName); err != nil {
		log.Panic("Chat room hasn't open %v ", err)
	}
}

// Room represents a single chat room
type ChatRoom struct {
	ChatRoomName  string
	publisherChan chan []byte
	subscribers   map[string](chan []byte)
}

var ChatRooms map[string]*ChatRoom = make(map[string]*ChatRoom)

var Upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 256,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// Getting char room by chatRoomName rom map ChatRooms

func GetChatRoom(chatRoomName string) (chatRoom *ChatRoom, err error) {

	if _, ok := ChatRooms[chatRoomName]; ok {
		// ok true
		log.Println("New chatter join to room ", chatRoomName)
	} else {
		log.Error("No Chatting Rooms have been started")
		err = errors.New("No Chatting Rooms have been started")

	}

	return ChatRooms[chatRoomName], err

}

func FromJSON(jsonInput []byte, v interface{}) error {
	return json.Unmarshal(jsonInput, v)

}

// Initial chatting room

func SetNewRoom(chatRoomName string) (chatRoom *ChatRoom, err error) {

	chatRoom = new(ChatRoom)
	chatRoom.ChatRoomName = chatRoomName

	log.Printf("Open new room : %q \n", chatRoom.ChatRoomName)
	chatRoom.publisherChan = make(chan []byte)
	chatRoom.subscribers = make(map[string](chan []byte))
	ChatRooms[chatRoomName] = chatRoom

	if chatRoom == nil ||
		chatRoom.publisherChan == nil ||
		chatRoom.subscribers == nil {
		return nil, errors.New("chatroom corrupted")
	}

	return chatRoom, nil

}

func PrintChatters(chatRoom *ChatRoom) {

	for i, sub := range chatRoom.subscribers {

		log.Printf("Online %v %v\n", i, sub)

	}
}

func BroadcastTextMessages(chatRoom *ChatRoom, msg []byte) {
	for _, sub := range chatRoom.subscribers {
		log.Println("Sending TEXT Message to all subcribers")
		sub <- msg
	}

	m, err := getHumMesasge(msg)
	if err != nil {
		log.Error("Error parsing message from array bytes : %v", err)

	}

	// send msg to Dynamo

	messages.PutMessageToDynamoHistory(&m)
	log.Printf("Sending message to Dynamo : %v", m)

}

func getHumMesasge(b []byte) (message messages.HumMessage, err error) {

	if err = FromJSON(b, &message); err != nil {
		return messages.HumMessage{}, err
	}

	return message, nil

}

func SaveFiletoLocal(b []byte, file []byte) []byte {

	message := &messages.HumMessage{}

	if err := FromJSON(b, &message); err != nil {
		log.Println(err)
		return nil
	}

	name := "./logs/" + message.MessageData.Text
	message.MessageData.TypeOfHumMessage = "Blob"

	if err := ioutil.WriteFile(name, file, 0644); err != nil {
		log.Error("File didn't write ", err)
	}

	b, err := json.Marshal(message)
	if err != nil {
		log.Error(err)
	}

	return b
}

func HandlerWs(w http.ResponseWriter, r *http.Request) {

	var (
		TypeMessage int
		lastMsg     []byte
	)

	conn, err := Upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("Upgrader failed with error : ", err)
		return
	} else {
		log.Println("Handler represented a websocket connection")
	}

	// getting chatroom
	chatRoom, err := GetChatRoom(ChatRoomName)
	if err != nil {
		m := "Chatting room isn't open"
		log.Error(m)

		// inform client about error
		conn.WriteMessage(websocket.CloseMessage, []byte(m))
		return
	}

	// id current websocket connection for each chatters. Storing in map chatRoom.subscribers
	id_ws := conn.RemoteAddr().String() + r.UserAgent()

	// Getting own chan for writting messages and add chan to map of all subscribers
	subscriber := make(chan []byte)
	chatRoom.subscribers[id_ws] = subscriber

	defer func() {

		log.Printf("chatter leaving room %v\n", id_ws)
		conn.Close()
		delete(chatRoom.subscribers, id_ws)
	}()

	// logging all online chatters
	PrintChatters(chatRoom)

	log.Printf(" User %v is starting chatting in %v\n", chatRoom)

	go func() {
		for {
			time.Sleep(pingPeriod)

			//Ping WebSocket connection websocket.PingMessage send

			if err := conn.WriteControl(websocket.PingMessage, []byte{}, time.Now().Add(pongWait)); err != nil {
				log.Printf("Keep-alive period write messages error ", err)

			}
		}
	}()

	go func() {
		for {

			// waiting writing new messages.......")

			data := <-subscriber

			log.Printf("Logging messages type : %v", TypeMessage)

			if TypeMessage == websocket.TextMessage {

				err := conn.WriteMessage(websocket.TextMessage, data)
				if err != nil {
					log.Printf("An error happened when WriteMessage: %v", err)
					break
				}
			}

			if TypeMessage == websocket.BinaryMessage {
				err := conn.WriteMessage(websocket.BinaryMessage, data)
				if err != nil {
					log.Printf("An error happened when WriteMessage: %v", err)

					break
				}
			}
		}

	}()

	for {

		conn.SetReadDeadline(time.Now().Add(pongWait))
		conn.SetPongHandler(func(string) error {
			conn.SetReadDeadline(time.Now().Add(pongWait))
			return nil
		})

		if messageType, msg, err := conn.ReadMessage(); err != nil {

			// need to get HumUser for send file
			lastMsg = msg

			if websocket.IsUnexpectedCloseError(err) {
				log.Printf("Unexpected Close WebSocket Conection: %v", err)

			} else {

				log.Printf("An error happened when Read Message: %v", err)
			}

			break

		} else {

			switch messageType {
			case websocket.CloseNormalClosure:

				log.Printf("Web Socket Normal Close: %v")

			case websocket.TextMessage:
				{

					log.Printf("New message recieved : %v", string(msg))
					TypeMessage = websocket.TextMessage
					BroadcastTextMessages(chatRoom, msg)
				}
			case websocket.BinaryMessage:
				{
					log.Printf("Recieved Binary Message of %d bytes", len(msg))

					// save file  and return HumMessage with link
					newMsg := SaveFiletoLocal(lastMsg, msg)

					TypeMessage = websocket.TextMessage

					BroadcastTextMessages(chatRoom, newMsg)
				}

			}

		}

	}

}

package server

import (
  "net/http"
  "github.com/gorilla/websocket"
  "go-team-room/models/context"
  "go-team-room/events"
)

var upgrader = websocket.Upgrader{
  ReadBufferSize:  1024,
  WriteBufferSize: 1024,
  HandshakeTimeout: 10000,
}

func webSocketEventHandler(w http.ResponseWriter, r *http.Request) {
  userId := context.GetIdFromContext(r)

  if websocket.IsWebSocketUpgrade(r) && userId != 0 {
    conn, err := upgrader.Upgrade(w, r, nil)
    if err != nil {
      responseError(w, err, http.StatusBadRequest)
      return
    }

    defer func() {
      conn.Close()
      events.EventsFlow <- &events.ControlEvent{events.EVENT_USER_DISCONNECTED, userId}
    }()

    events.EventsFlow <- &events.ControlEvent{events.EVENT_USER_CONNECTED, userId}

    for {
      _, _, err := conn.ReadMessage()
      if err != nil {
        log.Warn(err)
        break
      }
    }
  }
}

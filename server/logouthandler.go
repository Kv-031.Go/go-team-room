package server

import (
  "net/http"
  "fmt"
  "go-team-room/conf"
)

func logout(w http.ResponseWriter, r *http.Request) {
  session, err := store.Get(r, "name")

  if err != nil {
    responseError(w, err, http.StatusBadRequest)
    return
  }

  if session.Values["loginned"] != true || !sessionIsValid(session){
    fmt.Fprintf(w, "You are not loginned in!")
    return
  }

  session.Values["loginned"] = false
  session.Options.MaxAge = -1
  session.Options.Path = conf.DefaultPath
  session.Save(r, w)
  fmt.Fprintf(w, "User was logout")
}

// Copyright 2014 beego Author. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"fmt"
	"go-team-room/humstat"

	"net/http"

	"bytes"
	"encoding/json"
	"strconv"
	"time"
)

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

type RecordHTTPRequest struct {
	Name_route string
	Visit      int
	Start_time time.Time
	StatusCode *loggingResponseWriter
	Req        *http.Request
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

// struct for holding access log data.
type recHttpJson struct {
	Name_route     string `json:"route_name"`
	Number_request int    `json:"number_request"`
	Start_time     string `json:"start_time"`
	RemoteAddr     string `json:"remote_addr"`
	Request        string `json:"request"`
	StatusCode     int    `json:"status_code"`
	StatusText     string `json:"status_text"`
	ElapsedTime    string `json:"elapsed_time"`
	Host           string `json:"host"`
	HTTPReferrer   string `json:"http_referrer"`
	HTTPUserAgent  string `json:"http_user_agent"`
	RemoteUser     string `json:"remote_user"`
	XForwardedFor  string `json:"X-Forwarded-For"`
}

func (r *recHttpJson) json() ([]byte, error) {
	buffer := &bytes.Buffer{}
	encoder := json.NewEncoder(buffer)

	err := encoder.Encode(r)
	return buffer.Bytes(), err
}

var (
	cVisit   = 0
	cElapsed time.Duration
)

func logRecordRequest(lrh *RecordHTTPRequest) {

	req_msg := RecRequest(LogRequestRecord(lrh))

	log.Info(req_msg)

	humstat.SendStat <- map[string]int{
		"PageServed": 1,
	}

}

func RecRequest(recLogger *recHttpJson) (msg string) {

	if jsonData, err := recLogger.json(); err != nil {
		msg = fmt.Sprintf(`{"Error": "%s"}`, err)
		return
	} else {

		msg = string(jsonData)

	}

	return

}

func LogRequestRecord(lrh *RecordHTTPRequest) (recLogger *recHttpJson) {

	recLogger = &recHttpJson{
		Name_route:     lrh.Name_route,
		Number_request: lrh.Visit,
		Start_time:     lrh.Start_time.String(),
		RemoteAddr:     lrh.Req.RemoteAddr,
		Request:        fmt.Sprintf("%s %s %s", lrh.Req.Method, lrh.Req.RequestURI, lrh.Req.Proto),
		StatusCode:     lrh.StatusCode.statusCode,
		StatusText:     http.StatusText(lrh.StatusCode.statusCode),
		ElapsedTime:    time.Since(lrh.Start_time).String(),
		Host:           lrh.Req.Host,
		HTTPReferrer:   lrh.Req.Header.Get("Referer"),
		HTTPUserAgent:  lrh.Req.Header.Get("User-Agent"),
		RemoteUser:     lrh.Req.Header.Get("Remote-User"),
		XForwardedFor:  lrh.Req.Header.Get("X-Forwarded-For"),
	}

	return

}

func NewLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	// WriteHeader(int) is not called if our response implicitly returns 200 OK, so
	// we default to that status code.
	return &loggingResponseWriter{w, http.StatusOK}
}

func logRequest(next http.Handler, route_name string) http.HandlerFunc {

	//here will be done once for each route then started router

	return func(w http.ResponseWriter, req *http.Request) {

		cVisit++

		lrw := NewLoggingResponseWriter(w)

		lreq := &RecordHTTPRequest{
			Name_route: route_name,
			Visit:      cVisit,
			Start_time: time.Now(),
			StatusCode: lrw,
			Req:        req,
		}

		next.ServeHTTP(lrw, req)

		logRecordRequest(lreq)

	}

}

func FunNotFound(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/notFound", 301)

}

func notFound(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/", 301)
}

func handlerStats(w http.ResponseWriter, r *http.Request) {

	w.Write([]byte("=== Statistic page === \n"))
	w.Write([]byte("=== All requests : "))
	w.Write([]byte(strconv.Itoa(cVisit)))
	w.Write([]byte("\n"))
	w.Write([]byte("=== Summary time to handle your requests : "))
	w.Write([]byte(cElapsed.String()))
	w.Write([]byte("\n"))

}

  package messages

  import (
    "encoding/json"
    "errors"
    "fmt"
    "net/http"
    "strconv"
    "time"

    "go-team-room/conf"
    "go-team-room/humaws"
    "go-team-room/humstat"
    "go-team-room/models/context"

    "github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/service/dynamodb"
    "github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
    "github.com/aws/aws-sdk-go/service/dynamodb/expression"
  )

  //GetMessageFromDynamoByUserID - return an slise of latest
  //messages but not more then MAX_MESSAGES_AT_ONCE
  func GetMessageFromDynamoByUserID(humUserID int, chatID string, maxMessages ...int) []HumMessage {

    var resultMessages []HumMessage
    maxMes := conf.MaxMessages
    if maxMessages != nil && maxMessages[0] < maxMes {
      maxMes = maxMessages[0]
    }
    if humUserID < 1 {

      logRus.Debug("Some one tryed to get messages without ID.")
      return resultMessages //emty array

    }

    maxMess64 := int64(maxMes)
    // Build the query input parameters

    params := &dynamodb.QueryInput{
      ExpressionAttributeNames: map[string]*string{
        //"#0": aws.String("message_user"),
        //"#1": aws.String("id_sql"),
        "#2": aws.String("message_chat_room_id"),
      },
      ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
        //	":0": {
        //		N: aws.String(strconv.Itoa(humUserID)),
        //	},
        ":1": {
          S: aws.String(chatID),
        },
      },
      Limit: &maxMess64,
      //FilterExpression:       aws.String("#0.#1 = :0"),
      KeyConditionExpression: aws.String("#2 = :1"),
      TableName:              aws.String("messages"),
    }
    result, err := humaws.Dyna.Db.Query(params)

    if err != nil {
      logRus.Error("Query API call failed:")
      logRus.Error(err.Error())
      //fmt.Println(params)
      //os.Exit(1)
      return resultMessages //empty
    }
    var humMessageHolder HumMessage
    for _, i := range result.Items {

      err = dynamodbattribute.UnmarshalMap(i, &humMessageHolder)
      if err != nil {
        fmt.Println(err) //panic(err)
      } else {
        resultMessages = append(resultMessages, humMessageHolder)
      }
    }
    return resultMessages
  }

  //HandlerOfGetMessages - handler for
  //"/messages" endpoint
  func HandlerOfGetMessages(writeRespon http.ResponseWriter, r *http.Request) {
    ///Debug
    // GetChatRoomListByUserID(23, 23)
    // return
    ///DEBUG
    currentUserID := GetActionUserID(r)

    //fmt.Println(currentUserID)
    if currentUserID < 1 {
      writeRespon.WriteHeader(http.StatusUnauthorized)
      fmt.Fprint(writeRespon, "401 Can't find your ID")
      return
    }
    var numberOfMessages int
    keys, ok := r.URL.Query()["numberOfMessages"]
    if !ok || len(keys) < 1 {
      numberOfMessages = conf.MaxMessages
    } else {
      numberOfMessages, _ = strconv.Atoi(keys[0])
      if numberOfMessages > conf.MaxMessages || numberOfMessages < 1 {
        numberOfMessages = conf.MaxMessages
      }
    }

    var chatRoomId string
    keys, ok = r.URL.Query()["chatRoomId"]
    if !ok || len(keys) < 1 {
      chatRoomId = "hum_chat_room"
    } else {
      chatRoomId = keys[0]
    }

    usersMessagesObj := GetMessageFromDynamoByUserID(currentUserID, chatRoomId, numberOfMessages)

    _ = json.NewEncoder(writeRespon).Encode(&usersMessagesObj)
    return
  }

  //GetActionUserID - return ID of user who make this action
  func GetActionUserID(r *http.Request) int {
    currentUserID := int(context.GetIdFromContext(r))

    if currentUserID < 1 {
      return -1
    }
    return currentUserID
  }

  //ValidateDataFromUser very important func
  //Do NOT trust any data from User!!!
  //VALIDATE EVERETHING
  func ValidateDataFromUser(m *HumMessage, userId int) (respError error) {

    if m.MessageData.Text == "" && m.MessageData.BinaryParts[0].BinName == "" {
      //empty  messages not allowed
      respError = errors.New("empty  messages not allowed")
      return respError
    }
    //add some more data check later

    // if no problems until here
    return nil
  }

  //ReadReqBodyPOST - reads all POST request body to HumMessage
  func ReadReqBodyPOST(req *http.Request, humMess *HumMessage) {
    // body, err := ioutil.ReadAll(req.Body)
    //  if err != nil {
    //  	return //r1
    //  }
    // fmt.Println(string(body)) //DEBUG output
    var data HumMessage

    err := json.NewDecoder(req.Body).Decode(&data)
    fmt.Println(data)
    //err = json.Unmarshal(body, humMess)
    if err != nil {
      //panic(err)
      fmt.Println(err)
    }
  }

  //PutMessageToDynamoHistory get json HummMessage obj and write it to Dynamo
  //result of operation writed into nohwere
  func PutMessageToDynamoHistory(m *HumMessage) {
    //m.MessageUser.IdSql = currentUserID
    //m.MessageUser.NameSQL = context.GetFirstNameFromContext(r)
    m.MessageID = time.Now().String()
    m.MessageTimestamp = time.Now().String()
    if m.MessageChatRoomID == "" {
      m.MessageChatRoomID = "hum_chat_room" //DEFAULT chatroom id
    }
    m.MessageSocialStatus.Like = 0
    m.MessageSocialStatus.Dislike = 0
    m.MessageSocialStatus.Views = 0
    av, err := dynamodbattribute.MarshalMap(m)

    if err != nil {
      logRus.Error("Got error marshalling map:")
      logRus.Error(err.Error())
      //os.Exit(1)
    }

    // Create item in table messages
    input := &dynamodb.PutItemInput{
      Item:      av,
      TableName: aws.String("messages"),
    }

    _, err = humaws.Dyna.Db.PutItem(input)

    if err != nil {
      logRus.Info(err.Error())
      //os.Exit(1)
      humstat.SendStat <- map[string]int{
        "UserSendMessagesErrors": 1,
      }
    } else {
      humstat.SendStat <- map[string]int{
        "UserSendMessages": 1,
      }
    }
    //fmt.Println("Successfully added to  table")
  }

  //PutMessageToDynamo get prepeared HummMessage obj and write it to Dynamo
  //result of operation writed into writeRespon
  func PutMessageToDynamo(writeRespon http.ResponseWriter, m *HumMessage) {

    av, err := dynamodbattribute.MarshalMap(m)

    if err != nil {
      logRus.Error("Got error marshalling map:")
      logRus.Error(err.Error())
      //os.Exit(1)
    }

    // Create item in table messages
    input := &dynamodb.PutItemInput{
      Item:      av,
      TableName: aws.String("messages"),
    }

    _, err = humaws.Dyna.Db.PutItem(input)

    if err != nil {
      fmt.Println("Got error calling PutItem:")
      fmt.Println(err.Error())
      //os.Exit(1)
      writeRespon.WriteHeader(http.StatusInternalServerError)
      fmt.Fprint(writeRespon, "Some errors")
      humstat.SendStat <- map[string]int{
        "UserSendMessagesErrors": 1,
      }
    } else {
      writeRespon.WriteHeader(http.StatusOK)
      humstat.SendStat <- map[string]int{
        "UserSendMessages": 1,
      }
      b, err := json.Marshal(m)
      if err != nil {
        logRus.Error("error:", err)
      }
      fmt.Fprint(writeRespon, string(b))
    }
    //fmt.Println("Successfully added to  table")
  }

  //HandlerOfPOSTMessages This func should process any messages end point
  func HandlerOfPOSTMessages(w http.ResponseWriter, r *http.Request) {

    currentUserID := GetActionUserID(r)

    if currentUserID < 1 {
      w.WriteHeader(http.StatusUnauthorized)
      fmt.Fprint(w, "401 Can't find your ID")
      return
    }
    var inputMessage HumMessage
    err := json.NewDecoder(r.Body).Decode(&inputMessage)
    if err != nil {
      //panic(err)
      return
    }

    //CORS!!! "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --disable-web-security --user-data-dir="D:/Chrome"

    //Assume it is an POST
    //Shuold  expect a user message in form of
    /*
       curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{"message_chat_room_id": "997",
         "message_data": {
         "binary_parts": [{
       "bin_data": null,
        "bin_name": null }],
         "text": "A lot of text and stupid smiles :)))))",
        "type": "TypeOfHumMessage-UNDEFINED FOR NOW"},
         "message_id": "20180110155343152",
         "message_parent_id": "20180110155533289",
         "message_social_status": {
         "Dislike": 11,
         "Like": 22,
         "Views": 33 },
         "message_timestamp": "20180110155533111",
         "message_user": {
         "id_sql": 23,
         "name_sql": "Vasya" }
       }' 'http://localhost:8080/messages'
    */
    //// https://gist.github.com/alyssaq/75d6678d00572d103106

    //fmt.Println(m)

    inputMessage.MessageUser.IdSql = currentUserID
    inputMessage.MessageUser.NameSQL = context.GetFirstNameFromContext(r)
    inputMessage.MessageID = time.Now().String()
    inputMessage.MessageTimestamp = time.Now().String()
    if inputMessage.MessageChatRoomID == "" {
      inputMessage.MessageChatRoomID = "hum_chat_room" //DEFAULT chatroom id
    }
    inputMessage.MessageSocialStatus.Like = 0
    inputMessage.MessageSocialStatus.Dislike = 0
    inputMessage.MessageSocialStatus.Views = 0

    err = ValidateDataFromUser(&inputMessage, currentUserID)

    if err != nil {
      //is this wrong message? - chancel it
      w.WriteHeader(http.StatusBadRequest)
      fmt.Fprint(w, err)
    }

    //assume data validated
    //and it is safe to put it into a Dynamo

    PutMessageToDynamo(w, &inputMessage)
  }

  //GetChatRoomListByUserID - obviously, return lost of chatrooms, where userId inlisted
  func GetChatRoomListByUserID(humUserID int, maxChatRooms ...int) []HumChatRoom {
    var resultChatRooms []HumChatRoom
    maxChats := conf.MaxChatRooms
    if maxChatRooms != nil && maxChatRooms[0] < maxChats {
      maxChats = maxChatRooms[0]
    }
    if humUserID < 1 {
      return resultChatRooms //emty array
    }
    // Create the Expression to fill the input struct with.
    filt := expression.Name("chat_users_list[0].id_sql").Equal(expression.Value(humUserID))
    expr, err := expression.NewBuilder().WithFilter(filt).Build()

    if err != nil {
      logRus.Info("Got error building Dynamo expression")
      logRus.Error(err.Error()) //DEBUG output
      //os.Exit(1)
      return resultChatRooms //emty array
    }
    // Build the query input parameters
    params := &dynamodb.ScanInput{
      ExpressionAttributeNames:  expr.Names(),
      ExpressionAttributeValues: expr.Values(),
      FilterExpression:          expr.Filter(),
      //  ProjectionExpression:      expr.Projection(),
      TableName: aws.String("chat_rooms"),
    }
    result, err := humaws.Dyna.Db.Scan(params)
    if err != nil {
      logRus.Info("Query API call failed:")
      logRus.Error((err.Error()))
      //fmt.Println(params)
      //os.Exit(1)
      return resultChatRooms //empty
    }
    var humChatHolder HumChatRoom
    for _, i := range result.Items {
      err = dynamodbattribute.UnmarshalMap(i, &humChatHolder)
      if err != nil {
        fmt.Println(err) //panic(err)
      } else {
        resultChatRooms = append(resultChatRooms, humChatHolder)
      }
      fmt.Println("item.ChatID: ", i)
      //fmt.Println("item.ChatUsersList", i.ChatUsersList)
    }
    return resultChatRooms
  }

  ////////////
  func GetChatRoomUsersFromDynamoByChatID(chatID string) []HumUser {

    var resultUsers []HumUser

    // Build the query input parameters

    params := &dynamodb.QueryInput{
      ExpressionAttributeNames: map[string]*string{
        "#0": aws.String("chat_id"),
        //"#1": aws.String("chat_users_list"),
        //"#2": aws.String("user_sql_id"),
      },
      ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
        //	":0": {
        //		N: aws.String(strconv.Itoa(humUserID)),
        //	},
        ":0": {
          S: aws.String(chatID),
        },
      },
      //Limit:                  &maxMess64,
      //FilterExpression:       aws.String("#0.#1 = :0"),
      KeyConditionExpression: aws.String("#0 = :0"),
      TableName:              aws.String("chat_rooms"),
    }
    result, err := humaws.Dyna.Db.Query(params)

    if err != nil {
      logRus.Error("Query API call failed:")
      logRus.Error(err.Error())
      //fmt.Println(params)
      //os.Exit(1)
      return resultUsers //empty
    }
    var humUserHolder HumUser
    for _, i := range result.Items {

      err = dynamodbattribute.UnmarshalMap(i, &humUserHolder)
      if err != nil {
        fmt.Println(err) //panic(err)
      } else {
        resultUsers = append(resultUsers, humUserHolder)
      }
    }
    return resultUsers
  }

package controllers

import (
  "github.com/aws/aws-sdk-go/service/dynamodb"
  "github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
  "testing"
  "fmt"
  "strings"
  "github.com/aws/aws-sdk-go/service/s3/s3iface"
)

type mockDynamoDBClient struct {
  dynamodbiface.DynamoDBAPI
}

type mockS3Client struct {
  s3iface.S3API
}

func (m *mockDynamoDBClient) GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error){
  test := make(map[string]*dynamodb.AttributeValue)
  s1 := "title"
  s2 := "text"
  s3 := "user"
  s4 := "post id"
  s5 := "NULL"
  s6 := "last update"
  s7 := "0"

  test["post_title"] = &dynamodb.AttributeValue{S: &s1}
  test["post_text"] = &dynamodb.AttributeValue{S: &s2}
  test["user_id"] = &dynamodb.AttributeValue{S: &s3}
  test["post_id"] = &dynamodb.AttributeValue{S: &s4}
  test["file_link"] = &dynamodb.AttributeValue{S: &s5}
  test["post_last_update"] = &dynamodb.AttributeValue{S: &s6}
  test["post_like"] = &dynamodb.AttributeValue{S: &s7}

  output := dynamodb.GetItemOutput{
    Item: test,
  }
  return &output, nil
}

func (m *mockDynamoDBClient) PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error){
  test := make(map[string]*dynamodb.AttributeValue)
  s1 := "title"
  s2 := "text"
  s3 := "user"
  s4 := "post id"
  s5 := "NULL"
  s6 := "last update"
  s7 := "0"

  test["post_title"] = &dynamodb.AttributeValue{S: &s1}
  test["post_text"] = &dynamodb.AttributeValue{S: &s2}
  test["user_id"] = &dynamodb.AttributeValue{S: &s3}
  test["post_id"] = &dynamodb.AttributeValue{S: &s4}
  test["file_link"] = &dynamodb.AttributeValue{S: &s5}
  test["post_last_update"] = &dynamodb.AttributeValue{S: &s6}
  test["post_like"] = &dynamodb.AttributeValue{S: &s7}

  output := dynamodb.PutItemOutput{
    Attributes: test,
  }
  return &output, nil
}

func (m *mockDynamoDBClient) Scan(input *dynamodb.ScanInput) (*dynamodb.ScanOutput, error){
  test := make([]map[string]*dynamodb.AttributeValue, 1)
  temp := make(map[string]*dynamodb.AttributeValue)
  s1 := "title"
  s2 := "text"
  s3 := "user"
  s4 := "post id"
  s5 := "NULL"
  s6 := "last update"
  s7 := "0"

  temp["post_title"] = &dynamodb.AttributeValue{S: &s1}
  temp["post_text"] = &dynamodb.AttributeValue{S: &s2}
  temp["user_id"] = &dynamodb.AttributeValue{S: &s3}
  temp["post_id"] = &dynamodb.AttributeValue{S: &s4}
  temp["file_link"] = &dynamodb.AttributeValue{S: &s5}
  temp["post_last_update"] = &dynamodb.AttributeValue{S: &s6}
  temp["post_like"] = &dynamodb.AttributeValue{S: &s7}

  test = append(test, temp)
  output := dynamodb.ScanOutput{
    Items: test,
  }
  return &output, nil
}

func (m *mockDynamoDBClient) DeleteItem(*dynamodb.DeleteItemInput) (*dynamodb.DeleteItemOutput, error){
  output := dynamodb.DeleteItemOutput{}
  return &output, nil
}

func (m *mockDynamoDBClient) UpdateItem(*dynamodb.UpdateItemInput) (*dynamodb.UpdateItemOutput, error){
  test := make(map[string]*dynamodb.AttributeValue)
  s1 := "title"
  s2 := "text"
  s3 := "user"
  s4 := "post id"
  s5 := "NULL"
  s6 := "last update"
  s7 := "0"

  test["post_title"] = &dynamodb.AttributeValue{S: &s1}
  test["post_text"] = &dynamodb.AttributeValue{S: &s2}
  test["user_id"] = &dynamodb.AttributeValue{S: &s3}
  test["post_id"] = &dynamodb.AttributeValue{S: &s4}
  test["file_link"] = &dynamodb.AttributeValue{S: &s5}
  test["post_last_update"] = &dynamodb.AttributeValue{S: &s6}
  test["post_like"] = &dynamodb.AttributeValue{S: &s7}

  output := dynamodb.UpdateItemOutput{
    Attributes: test,
  }
  return &output, nil
}

func TestGetPost(t *testing.T) {
  var expectedResult Post
  expectedResult.UserID = "user"
  expectedResult.Title = "title"
  expectedResult.Text = "text"
  expectedResult.PostID = "post id"
  expectedResult.FileLink = "NULL"
  expectedResult.LastUpdate = "last update"
  expectedResult.Like = make([]*string, 0)
  initlike := "0"
  expectedResult.Like = append(expectedResult.Like, &initlike)

  svc := &mockDynamoDBClient{}
  id := "post id"
  gotPost, err := GetPost(svc, id)

  fmt.Println(gotPost)
  fmt.Println(expectedResult)

  if gotPost.PostID != expectedResult.PostID || gotPost.UserID != expectedResult.UserID || gotPost.Title != expectedResult.Title || gotPost.Text != expectedResult.Text || gotPost.FileLink != expectedResult.FileLink || gotPost.LastUpdate != expectedResult.LastUpdate || err != nil {
    fmt.Println("ERROR")
  }
}

func TestCreateNewPost(t *testing.T) {
  var expectedResult Post
  expectedResult.UserID = "user"
  expectedResult.Title = "title"
  expectedResult.Text = "text"
  expectedResult.PostID = "post id"
  expectedResult.FileLink = "NULL"
  expectedResult.LastUpdate = "last update"
  expectedResult.Like = make([]*string, 0)
  initlike := "0"
  expectedResult.Like = append(expectedResult.Like, &initlike)

  newpost := Post{
    Text: "text",
    Title: "title",
    PostID: "post id",
    UserID: "user id",
    FileLink: "NULL",
    LastUpdate: "last update",
    Like: make([]*string, 0),
  }
  newpost.Like = append(newpost.Like, &initlike)
  svc := &mockDynamoDBClient{}

  result, err := CreateNewPost(svc, newpost)

  if err == nil || result.PostID != expectedResult.PostID || result.UserID != expectedResult.UserID || result.Title != expectedResult.Title || result.Text != expectedResult.Text || result.FileLink != expectedResult.FileLink || result.LastUpdate != expectedResult.LastUpdate {
    fmt.Println("Error")
  }
}

func TestGetPostByUserID(t *testing.T) {
  var expectedResultArr []Post
  var expectedResult Post
  expectedResult.UserID = "user"
  expectedResult.Title = "title"
  expectedResult.Text = "text"
  expectedResult.PostID = "post id"
  expectedResult.FileLink = "NULL"
  expectedResult.LastUpdate = "last update"
  expectedResult.Like = make([]*string, 0)
  initlike := "0"
  expectedResult.Like = append(expectedResult.Like, &initlike)

  expectedResultArr = append(expectedResultArr, expectedResult)

  svc := &mockDynamoDBClient{}

  id := "user"

  result, err := GetPostByUserID(svc, id)

  for i:=0; i< len(expectedResultArr); i++{
    if result[i].PostID != expectedResult.PostID || result[i].UserID != expectedResult.UserID || result[i].Title != expectedResult.Title || result[i].Text != expectedResult.Text || result[i].FileLink != expectedResult.FileLink || result[i].LastUpdate != expectedResult.LastUpdate || err != nil {
      fmt.Println("Error")
    }
  }
}

func TestDeletePost(t *testing.T) {
  var expectedResult Post
  expectedResult.PostID = "post_id"
  expectedResult.FileLink = "NULL"
  svcd := &mockDynamoDBClient{}
  svcs := &mockS3Client{}
  result := DeletePost(svcd, svcs, expectedResult)

  if !strings.EqualFold(result, expectedResult.PostID) {
    fmt.Println("Error")
  }
}

func TestUpdatePost(t *testing.T) {
  var expectedResult Post
  expectedResult.UserID = "user"
  expectedResult.Title = "title"
  expectedResult.Text = "text"
  expectedResult.PostID = "post id"
  expectedResult.FileLink = "NULL"
  expectedResult.LastUpdate = "last update"
  expectedResult.Like = make([]*string, 0)
  initlike := "0"
  expectedResult.Like = append(expectedResult.Like, &initlike)

  newpost := Post{
    Text: "text",
    Title: "title",
    PostID: "post id",
    UserID: "user id",
    FileLink: "NULL",
    LastUpdate: "last update",
    Like: make([]*string, 0),
  }
  newpost.Like = append(newpost.Like, &initlike)

  svc := &mockDynamoDBClient{}

  result, _ := UpdatePost(svc, newpost, false)

  if result.PostID != expectedResult.PostID || result.UserID != expectedResult.UserID || result.Title != expectedResult.Title || result.Text != expectedResult.Text || result.FileLink != expectedResult.FileLink || result.LastUpdate != expectedResult.LastUpdate {
    fmt.Println("Error")
  }
}

func TestNewUUID(t *testing.T) {
  expectedResult := 36
  result, err := NewUUID()

  if err != nil || expectedResult != len(result){
    fmt.Println("Error")
  }
}



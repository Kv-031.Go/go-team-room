package events

import "testing"

func TestHandleUserConnected(t *testing.T) {
  tests := []struct{
    description string
    onlineUsers map[int64] int8
    ev *ControlEvent
    expOnlineUsers map[int64] int8
  } {
    {
      description: "should perform successfuly",
      onlineUsers: map[int64]int8{},
      ev: &ControlEvent{EVENT_USER_CONNECTED, int64(1)},
      expOnlineUsers: map[int64]int8{ 1: 1},
    },
  }

  for index, tc := range tests {
    handleUserConnected(tc.onlineUsers, tc.ev)
    for key, value := range tc.expOnlineUsers {
      if tc.onlineUsers[key] != value {
        t.Errorf("\nTEST CASE #%d\nExpected: %v \nBut got: %v", index, tc.expOnlineUsers, tc.onlineUsers)
      }
    }
  }
}

func TestHandleUserDisconected(t *testing.T) {
  tests := []struct{
    description string
    onlineUsers map[int64] int8
    ev *ControlEvent
    expOnlineUsers map[int64] int8
  } {
    {
      description: "should perform successfuly",
      onlineUsers: map[int64] int8 {1: 1},
      ev: &ControlEvent{EVENT_USER_DISCONNECTED, int64(1)},
      expOnlineUsers: map[int64]int8{},
    },
  }

  for index, tc := range tests {
    handleUserDisconnected(tc.onlineUsers, tc.ev)
    for key, value := range tc.expOnlineUsers {
      if tc.onlineUsers[key] != value {
        t.Errorf("\nTEST CASE #%d\nExpected: %v \nBut got: %v", index, tc.expOnlineUsers, tc.onlineUsers)
      }
    }
  }
}

"use strict";

import Vue from 'vue'
import VueRouter from 'vue-router'

import AppStartPage from '../Components/StartPage.vue'
import AppSignUp from '../Components/SignUp.vue'
import AppSignIn from '../Components/SignIn.vue'
import AppRecoveryPass from '../Components/RecoveryPass.vue'
import AppProfile from '../Components/Profile.vue'
import TopMenu from '../Components/TopMenu.vue'

import Friends from '../Components/Friends.vue'
import Messages from '../Components/Messages.vue'


Vue.component('AppSignUp', AppSignUp)
Vue.component('AppSignIn', AppSignIn)
Vue.component('AppRecoveryPass', AppRecoveryPass)
Vue.component('StartPage', AppStartPage)
Vue.component('Profile', AppProfile)
Vue.component('TopMenu', TopMenu)

Vue.component('Friends', Friends)
Vue.component('Messages', Messages)


Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/profile/:id',
      name: 'Profile',
      component: AppProfile
    },
    {
      path: '/',
      name: 'StartPage',
      component: AppStartPage
    },
    {
      path: '/friends',
      name: 'Friends',
      component: Friends
    },
    {
      path: '/messages',
      name: 'Messages',
      component: Messages
    },
  ]
})

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    isConnected: false,
    websocket: {},
    isConnectedMessages: false,
    websocket_messages: {},
    messages: []
  },
  mutations: {
    create(state, sock) {
      if (state.isConnected === false) {
        state.websocket = sock;
        state.isConnected = true
      }
    },
    create_messages(state, sock) {
      if (state.isConnectedMessages === false) {
        state.websocket_messages = sock;
        state.isConnectedMessages = true
      }
    },
    add_message(message) {
      state.messages.push(message);
    }
  },
  getters: {
    ws_messages: state => {
      return state.websocket_messages;
    },
    messages: state => {
      return state.messages;
    }
  }
});
